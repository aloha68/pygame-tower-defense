#!/usr/bin/env python3
# coding: utf-8
#
# Jeu de "Tower Defense" sans prétentions
# Have fun! :-)
#
# pip install pygame
#
# Ressources utiles :
#   https://fr.wikibooks.org/wiki/Pygame
#   http://www.pygame.org/docs/
#   
# Pathfinding :
#   http://www.redblobgames.com/pathfinding/tower-defense/
#   https://stackoverflow.com/questions/14298389/applying-pathfinding-algorithm-tower-defense-in-c
#   
#


# Démarrage du timer pour l'affichage des lignes de débug
import time
start_time = time.process_time()


from drawables.utils import *
from drawables.Drawable import Screen
from drawables.GameFrame import GameFrame
from drawables.Bars import InfosBar, TitleBar

from config import *
from models.level import *


import sys
import random
import threading
import pygame
from pygame.locals import *


class Info:
    pass


class InfoLevel(Info):
    """
    Classe d'informations qui sera transmise à la couche graphique pour mise à jour.
    """

    def __init__(self, level):

        self.golds = level.current_golds
        self.lifes = level.current_lifes
        self.starting_lifes = STARTING_LIFES

        wave = level.get_current_wave()

        self.wave_number = wave.num
        self.current_monsters = level.current_monsters
        self.coming_monsters = len(wave.monsters)

        self.time = round(level.current_time / 1000, 2)
        self.score = 0

        self.is_game_over = False


class GameScreen(Screen):
    """Classe représentant la fenêtre de jeu"""

    is_init = False
    is_done = False
    monster_cooldown = 0

    # Objets du jeu
    level = None
    end_of_the_wave = 3
    
    # Objets graphiques
    game_frame = None
    infos_bar = None
    
    def __init__(self, size, level):
        """Initialisation de la fenêtre de jeu"""
        debug("GameScreen: Init {}".format(size))
        Screen.__init__(self, size)

        self.level = level
        self.level.get_next_wave()

        self.init_screen()

    def init_screen(self):
        """Dessine tous les composants de l'écran"""

        debug("GameScreen: Init game frame and infos bar")

        # Taille du niveau en pixel
        width = 25 * BLOCK_SIZE
        height = 16 * BLOCK_SIZE

        # Création des objets
        self.game_frame = GameFrame((width, height))
        self.infos_bar = InfosBar((width, 38))

        # On configure l'écran de jeu en ajoutant tous les blocs
        for pos, block_type in self.level.loop_ground():
            self.game_frame.add_block(pos, block_type)

    def draw(self):
        """Dessine les objets à l'écran"""

        if not self.game_frame or not self.infos_bar:
            self.init_screen()

        # Position de la barre d'informations
        infos_position = self.infos_bar.get_rect()
        infos_position.bottomleft = self.get_rect().bottomleft

        self.blit(self.game_frame, (0, 0))
        self.blit(self.infos_bar, infos_position)

    def is_wave_finished(self):
        """
        Retourne True si la vague courante est finie.
        Ajout d'un timout avec la variable self.end_of_the_wave afin de
        permettre un affichage correct (le dernier monstre reste affiché sinon)
        """

        if self.level.wave_finished:
            if self.end_of_the_wave == 0:
                return True

            self.end_of_the_wave -= 1

        else:
            self.end_of_the_wave = 3

    def update_level(self, elapsed_time):
        """Mise à jour du niveau actuel"""
        
        level = self.level

        level.current_time += elapsed_time

        self.monster_cooldown += elapsed_time / 1000
        if self.monster_cooldown >= MONSTER_COOLDOWN:
            self.monster_cooldown = 0

            monster = self.level.release_monster()
            if monster:
                debug("GameScreen: Release new monster here")
                self.game_frame.add_monster(monster)

        # On vérifie si un monstre est sorti
        if self.game_frame.monsters:
            for monster in self.game_frame.monsters:
                if pygame.sprite.spritecollideany(monster, self.game_frame.exits):
                    monster.kill()
                    self.level.remove_life()

        # C'est ici qu'on va vérifier toutes les collisions !!!
        # Comme ça on peut mettre à jour directement le niveau, et l'envoie au graphisme par le biais de InfoLevel :)

        # # On met à jour le niveau à partir du graphisme affiché
        # if not self.is_wave_finished():
        #     self.frame.update_level(level)
        #
        #level.current_golds += random.randint(1, 10)
    
    def update(self, elapsed_time):
        """Mise à jour de l'affichage"""

        self.update_level(elapsed_time)

        info = InfoLevel(self.level)

        self.infos_bar.update(info)
        self.game_frame.update(info)

        self.draw()

    def handle_event(self, event):
        """Gestion des événements interceptés"""

        # Bouton de la souris enfoncé
        if event.type == pygame.MOUSEBUTTONUP:

            position = pygame.mouse.get_pos()
            debug("GameScreen: Mouse click on {}".format(position))

            position = (position[0], position[1] - 50) # On met à jour la position pour qu'elle corresponde à la fenêtre de jeu

            for block in self.game_frame.blocks:
                sprite = block.rect.collidepoint(position)

                if sprite:
                    x = round(position[0] / BLOCK_SIZE)
                    y = round(position[1] / BLOCK_SIZE)
                    debug("  Mouse click on wall on {}".format((x, y)))

        else:
            return False

        return True


class AlohaTowerDefense:
    """
    Classe principale de mon petit Tower Defense maison :-)
    À voir jusqu'à où ce dernier peut aller !
    """

    size = (800, 600)
    current_screen = None
    
    def __init__(self):
        """Initialisation de la fenêtre"""
        debug("Opening {} v{}! Welcome mate :-)".format(APPNAME, APPVERSION))
        pygame.init()

    def run(self):
        """
        Lancement du jeu Aloha Tower Defense :-)
        Boucle infinie tant qu'on ne ferme pas la fenêtre de jeu
        """

        debug("ATD: Screen init")

        # Initialisation de l'écran à partir des infos du jeu
        screen = pygame.display.set_mode(self.size)

        pygame.display.set_icon(load_png('pirate_flag.png')[0])
        pygame.display.set_caption("Welcome on {}".format(APPNAME))

        # Initialisation de la barre de titre
        debug("ATD: Title bar init")
        title_bar = TitleBar((self.size[0], 50), APPNAME)
        screen.blit(title_bar, (0, 0))

        # Initialisation du jeu
        debug("ATD: Game screen init")
        self.current_screen = GameScreen((self.size[0], self.size[1] - 50), DefaultLevel())
        screen.blit(self.current_screen, (0, 50))

        # Mise en place d'un timer pour gérer le nombre d'images par secondes
        second = 1000
        elapsed_time = 0
        clock = pygame.time.Clock()

        is_done = False
        debug("ATD: Starting main loop, {} frames per seconds".format(FPS))

        # Boucle de jeu
        while not is_done:
            
            # Gestion des événements
            for event in pygame.event.get():

                handle = False
                if self.current_screen:
                    handle = self.current_screen.handle_event(event)

                if not handle:
                    if event.type == QUIT:
                        is_done = True
                    elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                        is_done = True

            # Si une seconde s'est écoulée
            if second < 0:
                second = 1000
                pygame.display.set_caption("{} ({:.2f} FPS)".format(APPNAME, clock.get_fps()))

            # Mise à jour de l'écran et redessinage
            self.current_screen.update(elapsed_time)
            screen.blit(self.current_screen, (0, 50))

            pygame.display.update()

            # Gestion des temps
            elapsed_time = clock.tick(FPS) # fps maximum
            second -= elapsed_time
            
        pygame.quit()
        sys.exit()


def debug(*args):
    """Méthode permettant de débugguer"""
    elapsed_time = time.process_time() - start_time
    print('[DBG %f]:' % elapsed_time, *args)


def init():
    """Initialise ce qu'il faut :-)"""

    def debug(*args):
        """Méthode permettant de débugguer"""
        elapsed_time = time.process_time() - start_time
        print('[DBG %f]:' % elapsed_time, *args)

    # Initialisation du débogage
    import drawables
    drawables.init_debug(debug)

    from models import init_debug
    init_debug(debug)



if __name__ == '__main__':
    init()
    AlohaTowerDefense().run()
