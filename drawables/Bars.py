#!/usr/bin/env python3

from drawables import debug
from drawables.utils import *
from drawables.Drawable import DrawableSurface

from pygame.locals import *


class TitleBar(DrawableSurface):
    """Classe représentant la barre de commandes"""

    def __init__(self, size, title):
        """Initialisation de la barre de commandes"""
        DrawableSurface.__init__(self, size)

        # Couleur de fond
        self.fill(Color('white'))

        # Texte reprenant le titre passé en paramètre
        text = self.create_text(title)
        text_position = text.get_rect()
        text_position.centerx = self.get_rect().centerx
        text_position.centery = self.get_rect().centery
        self.blit(text, text_position)


class InfosBar(DrawableSurface):
    """Classe représentant la barre d'informations"""

    current_left = 0

    def __init__(self, size):
        """Initialisation de la barre d'informations"""
        DrawableSurface.__init__(self, size)

        self.fill(Color('white'))

    def draw_text(self, info, right_space=20):
        """Créer un texte contenant une information et le place à droite du dernier"""

        text = self.create_text(str(info))
        text_position = text.get_rect()
        text_position.left = self.current_left
        text_position.centery = self.get_rect().centery

        self.blit(text, text_position)
        self.current_left = text_position.right + right_space

    def draw_image(self, image, right_space=10):
        """Créer une image et l'ajoute à droite du dernier"""

        img, img_position = load_png(image)
        img_position.left = self.current_left
        img_position.centery = self.get_rect().centery

        self.blit(img, img_position)
        self.current_left = img_position.right + right_space

    def update(self, info):
        """Mise à jour de la barre d'informations"""

        self.fill(Color('white'))

        # On remet la valeur de cette variable à jour
        self.current_left = self.get_rect().left + 10

        if info.is_game_over:
            self.draw_image('rip.png')
            self.draw_text("Game over !")

            self.draw_image('star.png')
            self.draw_text("Score : {}".format(info.score))
            return

        # Vies
        self.draw_image('personals.png')
        self.draw_text(info.lifes)

        # Argent
        self.draw_image('coin_stack_gold.png')
        self.draw_text(info.golds)

        # Vague
        self.draw_image('bus.png')
        self.draw_text(info.wave_number)

        # Monstres
        self.draw_image('user_death.png')
        self.draw_text("{} / {}".format(info.current_monsters, info.coming_monsters))

        # Temps écoulé
        self.draw_image('time.png')
        self.draw_text("{}".format(info.time))