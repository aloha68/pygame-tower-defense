#!/usr/bin/env python3

from drawables.DrawableBlocks import *
from drawables.DrawableMonster import *


class BlockType:
    """Énumération regroupant tous les types de blocs disponibles dans l'édition de terrain."""

    WALL = 'B'
    PATH = ' '
    ENTRANCE = 'I'
    EXIT = 'O'

    @staticmethod
    def get_all():
        return BlockType.WALL \
               + BlockType.PATH \
               + BlockType.ENTRANCE \
               + BlockType.EXIT

    @staticmethod
    def is_valide(block_type):
        return block_type in BlockType.get_all()


class DrawableBuilder:

    @staticmethod
    def create_block(position, *groups):
        return DrawableBlock(position, *groups)

    @staticmethod
    def create_path(position, *groups):
        return DrawablePath(position, *groups)

    @staticmethod
    def create_entrance(position, *groups):
        return DrawableEntrance(position, *groups)

    @staticmethod
    def create_exit(position, *groups):
        return DrawableExit(position, *groups)

    @staticmethod
    def create_monster(position, *groups):
        return DrawableMonster()
