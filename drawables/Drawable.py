#!/usr/bin/env python3


import pygame
from pygame.locals import *

from drawables import *
from drawables.utils import *


class Screen(pygame.Surface):
    """Représente un écran dans l'application"""

    def __init__(self, size):
        pygame.Surface.__init__(self, size)

    def handle_event(self, event):
        """Gestion des événements interceptés"""
        return False

    def update(self, seconds):
        """Met à jour l'écran principal durant la boucle de jeu"""
        pass


class DrawableSurface(pygame.Surface):
    """Représente une surface dessinable"""

    def __init__(self, size):
        pygame.Surface.__init__(self, size)

    def create_text(self, text, size=16, color='black'):
        """Retourne une surface contenant le texte passé en paramètre"""

        font = pygame.font.SysFont('Liberation Sans', size)
        created_text = font.render(text, 1, Color(color))
        created_text = created_text.convert_alpha()
        return created_text

    def create_image(self, image):
        """Retourne une surface contenant le fichier dont le nom est passé en paramètre"""
        return load_png('{}.png'.format(image))

    def update(self, info):
        """Méthode pour mettre à jour le composant"""
        pass


class Drawable(pygame.sprite.Sprite):
    """Représente un objet dessinable"""

    x = y = 0
    line = column = 0

    def __init__(self, position, image, *groups):
        pygame.sprite.Sprite.__init__(self, *groups)

        self.line = position[0]
        self.column = position[1]

        self.x = self.column * BLOCK_SIZE
        self.y = self.line * BLOCK_SIZE

        if not image:
            raise Exception("Aucune image pour le bloc")

        # Chargement de l'image
        self.image, self.rect = load_png(image)
        self.rect.topleft = self.get_position()

    def get_position(self):
        """Retourne la position du Sprite"""
        return (self.x, self.y)

    def update(self, info):
        """Met à jour le bloc"""
        pass


class MovableDrawable(Drawable):
    """Représente un objet dessinable qui peut se déplacer"""

    def __init__(self, position, image, *groups):
        Drawable.__init__(self, position, image, *groups)

    def get_opposite_direction(self, direction):
        """"Retourne la direction opposée"""

        if direction == 'up':
            return 'down'
        elif direction == 'down':
            return 'up'
        elif direction == 'left':
            return 'right'
        elif direction == 'right':
            return 'left'