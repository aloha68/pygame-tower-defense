#!/usr/bin/env python3

from drawables.utils import load_png
from drawables.Drawable import Drawable


class DrawableBlock(Drawable):
    """Représente un bloc inoccupé du terrain"""

    def __init__(self, position, *groups):
        Drawable.__init__(self, position, 'flower.png', *groups)


class DrawablePath(Drawable):
    """Représente une partie du chemin du terrain"""

    def __init__(self, position, block_type, *groups):
        self.block_type = block_type
        Drawable.__init__(self, position, '', *groups)


class DrawableEntrance(Drawable):
    """Représente une entrée dans le niveau"""

    def __init__(self, position, *groups):
        Drawable.__init__(self, position, 'door_open.png', *groups)

    def update(self, info):

        if info.coming_monsters <= 0:
            self.image, tmp = load_png('door.png')


class DrawableExit(Drawable):
    """Représente une sortie dans le niveau"""

    def __init__(self, position, *groups):
        Drawable.__init__(self, position, 'house_two.png', *groups)

    def update(self, info):

        image = None

        # On réduit le nombre de vie initial en pourcentage
        # percent = info.starting_lifes / 100

        # starting_life = config.LevelConfiguration.STARTING_LIFES
        #
        # image = None
        # if current_life <= 0:
        #     image = 'fire.png'
        # elif current_life <= 2:
        #     image = 'evacuator.png'
        # elif current_life <= starting_life / 3:
        #     image = 'house.png'
        # elif current_life <= starting_life / 3 * 2:
        #     image = 'house_one.png'
        # else:
        #     image = 'house_two.png'
        #
        # if image:
        #     self.image, tmp = load_png(image)


class DrawableTurret(Drawable):
    """Représente une tourelle"""

    def __init__(self, position):
        Drawable.__init__(self, position)