#!/usr/bin/env python3

from drawables import debug
from drawables.utils import *
from drawables.Drawable import DrawableSurface
from drawables.DrawableBuilder import *

import pygame
from pygame.locals import *


class GameFrame(DrawableSurface):
    """Classe représentant la zone de jeu"""

    blocks = pygame.sprite.RenderPlain()
    paths = pygame.sprite.RenderPlain()
    exits = pygame.sprite.RenderPlain()
    monsters = pygame.sprite.RenderPlain()
    bullets = pygame.sprite.RenderPlain()
    collides_sprites = pygame.sprite.RenderPlain()

    def __init__(self, size):
        """Initialisation de la zone de jeu"""
        DrawableSurface.__init__(self, size)
        debug("GameFrame initialize: {}".format(size))

    def add_block(self, position, block_type):
        """Ajoute un bloc quelconque sur le terrain"""

        if block_type == BlockType.WALL:
            DrawableBuilder.create_block(position, [self.blocks, self.collides_sprites])
        elif block_type == BlockType.PATH:
            pass
            #DrawableBuilder.create_path(position, [self.paths])
        elif block_type == BlockType.ENTRANCE:
            DrawableBuilder.create_entrance(position, [self.paths])
        elif block_type == BlockType.EXIT:
            DrawableBuilder.create_exit(position, [self.paths, self.exits])

    def add_monster(self, monster):
        """Ajoute un monstre sur le terrain"""

        DrawableMonster(monster, [self.monsters])

    def update_level(self, level):
        """Mise à jour du niveau à partir des données affichées"""

        should_update_paths = False

        wave = level.get_current_wave()
        if not wave.monsters:
            for paths in self.paths:
                paths.close_door()
            should_update_paths = True

        # S'il y a au moins un monstre
        if self.monsters:

            # Mise à jour des informations du monstre
            for monster in self.monsters:

                # Si le monstre est sorti
                if monster.is_exit(self.exits):
                    level.remove_life()
                    monster.kill()
                    should_update_paths = True
                    for paths in self.paths:
                        paths.lose_life(level.current_lifes)
                    continue

                monster.update_position(self.collides_sprites)

            # On s'assure qu'il y a toujours des monstres
            if not self.monsters:
                level.wave_finished = True

    def update(self, info):
        """Mise à jour de la zone de jeu"""

        self.fill(Color('white'))

        if self.blocks:
            self.blocks.draw(self)

        if self.paths:
            self.paths.draw(self)

        if self.monsters:

            for monster in self.monsters:
                monster.update_position(self.collides_sprites)
            self.monsters.update()
            self.monsters.draw(self)
