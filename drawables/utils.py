#!/usr/bin/env python3

import os
import math
import random
import pygame


def radians_to_degrees(radians):
    return (radians / math.pi) * 180.0


def degrees_to_radians(degrees):
    return degrees * (math.pi / 180.0)


def load_png(name):
    """Charge une image et tourne un objet image"""

    fullname = os.path.join('images', name)

    try:
        image = pygame.image.load(fullname)
        if image.get_alpha() is None:
            image = image.convert()
        else:
            image = image.convert_alpha()

    except pygame.error as message:
        print("Can't load image: %s" % fullname)
        raise SystemExit(message)

    return image, image.get_rect()


def get_random_color():
    return random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)