#!/usr/bin/env python3

APPNAME = "Aloha's Tower Defense"
APPVERSION = '0.1'

FPS = 60
BLOCK_SIZE = 32

# Game configuration
STARTING_LIFES = 20
STARTING_GOLDS = 500
MONSTER_COOLDOWN = 1