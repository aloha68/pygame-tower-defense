#!/usr/bin/env python3

from models import debug
from drawables.DrawableBuilder import BlockType

import time
import config
import random


class Wave:
    """
    Représente une vague de monstres.
    """

    monsters = []

    def __init__(self, num):
        """Initialisation de la vague"""
        self.num = num
        self.monsters = []

    def add_monster(self, monster):
        self.monsters.append(monster)


class Level:
    """
    Représente un niveau de jeu.
    Il regroupe les informations sur les objets présents sur le niveau
    """

    # Liste des entrées et des sorties
    entrances = []
    exits = []

    # Carte détaillée du terrain (dictionnaire, key = (x, y), value = BlocType)
    ground = {}

    # Liste des monstres actuellement sur la carte
    current_monsters = 0

    # Informations sur les vagues
    waves = {}
    current_wave = 0
    wave_finished = True

    # Infos sur le niveau en cours
    score = 0
    start_time = 0
    current_time = 0

    current_golds = config.STARTING_GOLDS
    current_lifes = config.STARTING_LIFES

    is_finished = False
    is_game_over = False

    def __init__(self, path):
        """
        Initialisation d'un niveau de jeu.
        Pour le moment, on se content d'un parcours tracé en variable de classe :-)
        """

        self.path = path

        # Nombre de ligne et de colonnes
        self.width = max([len(str(line)) for line in self.path])
        self.height = len(self.path)

        # Initialisation du terrain
        self._init_ground()

    def _init_ground(self):
        """
        Initialise le tableau de terrain.
        Cette méthode initialise le dictionnaire self.ground
            Key: (ligne, colonne) où chaque nombre débute à 0
            Value: valeur de BlocType
        """

        debug("Level: Creating level from text")

        currentLine = currentColumn = 0

        # Pour chaque ligne du niveau
        for line in self.path:

            # Pour chaque colonne de la ligne courante
            for block_type in line:

                self.ground[(currentLine, currentColumn)] = block_type

                if block_type == BlockType.ENTRANCE:
                    self.entrances.append((currentLine, currentColumn))
                elif block_type == BlockType.EXIT:
                    self.exits.append((currentLine, currentColumn))

                currentColumn += 1

            currentLine += 1
            currentColumn = 0

        debug("Level: Creation done")
        debug("  Size: ({}, {}), Blocks: {}".format(self.width, self.height, len(self.ground)))
        debug("  Entrances in {}".format(self.entrances))
        debug("  Exits in {}".format(self.exits))

    def loop_ground(self):
        """
        Permet d'itérer sur la liste des blocs du niveau
        Retourne un résultat sous la forme de : ((x, y), type)
            (x, y) : tuple contenant numéro de ligne et colonne (débutent à 0)
            type : type du bloc courrant
        """
        for position, block_type in self.ground.items():
            yield position, block_type

    def _print_level(self):
        """
        Imprime le niveau en mode console :-)
        """

        x = y = 0
        for pos, block_type in self.loop_ground():
            if pos[0] > x:
                print()
            x, y = pos
            print(block_type, end='')
        print()

    def get_entrance(self):
        """Retourne une entrée aléatoire"""
        return random.choice(self.entrances)

    def get_exit(self):
        """Retourne une sortie aléatoire"""
        return random.choice(self.exits)

    def build_turret(self, line, column, turret):
        pass

    def add_wave(self, wave):
        """Ajoute une vague de monstre au niveau"""

        num = len(self.waves) + 1
        self.waves[num] = wave

    def get_current_wave(self):
        """Récupère la vague en cours"""

        if not self.current_wave in self.waves:
            self.wave_finished = True
            self.is_finished = True
            return

        wave = self.waves[self.current_wave]
        return wave

    def get_next_wave(self):
        """Récupère la prochaine vague"""

        self.current_monsters = 0
        self.current_wave += 1
        self.wave_finished = False

        if self.current_wave == 1:
            start_time = time.process_time()

        return self.get_current_wave()

    def get_current_time(self):
        elapsed_time = time.process_time() - self.start_time
        return round(elapsed_time, 2)

    def release_monster(self):
        """Libère un monstre de la vague"""

        wave = self.get_current_wave()
        if not wave:
            return

        if not wave.monsters:
            return

        self.current_monsters += 1

        monster = wave.monsters.pop()
        return monster

    def remove_life(self):
        self.current_lifes -= 1
        self.current_monsters -= 1

        if self.current_lifes <= 0:
            self.is_finished = True
            self.is_game_over = True

        if self.current_monsters <= 0:
            self.wave_finished = True


class DefaultLevel(Level):
    """Niveau par défaut proposant un chemin pré-défini"""

    def __init__(self):
        path = [

            # ~ "BBBOBBB",
            # ~ "BBB BBB",
            # ~ "I     I",

            # ~ "BBBBBBBBBBB",
            # ~ "B   B   B O",
            # ~ "B B B B B B",
            # ~ "I B   B   B",
            # ~ "BBBBBBBBBBB",

            # ~ "BBBBBBBBBBBBBBBBBBBB",
            # ~ "I  BB    BB    BB  O",
            # ~ "BB BB BB BB BB BB BB",
            # ~ "BB BB BB BB BB BB BB",
            # ~ "BB BB BB BB BB BB BB",
            # ~ "BB    BB    BB    BB",
            # ~ "BBBBBBBBBBBBBBBBBBBB",

            # "BBBBBBBBBBBBBBBBBBBBB",
            # "B       B       B   B",
            # "B BBBBB B BBBBB B B B",
            # "B     B B     B B B B",
            # "BBBBB B BBBBB B B B B",
            # "I     B       B   B O",
            # "BBBBBBBBBBBBBBBBBBBBB",

            "BBBBBBBBBBBBBBBBBBBBBBBBB",
            "I      B                B",
            "BBBBBB B BBBBBBBBBBBBBB B",
            "B    B B B            B B",
            "B BB B B B BBBBBBBBBB B B",
            "B BB   B B          B B B",
            "B BBBBBB BBBBBBBBBB B B B",
            "B        BB   B   B B B B",
            "BBBBBBBBBBB B B B B B B B",
            "B           B   B   B   B",
            "B BBBBBBBBBBBBBBBBBBBBBBB",
            "B B   B   B   B   B   B O",
            "B B B B B B B B B B B B B",
            "B B B B B B B B B B B B B",
            "B   B   B   B   B   B   B",
            "BBBBBBBBBBBBBBBBBBBBBBBBB",

            # ~ "BBOBBBBBBBBBBBOBB",
            # ~ "BB B   BBB   B BB",
            # ~ "B  B B BBB B B  B",
            # ~ "B BB B BBB B BB B",
            # ~ "B    B     B    B",
            # ~ "BBBBBBBB BBBBBBBB",
            # ~ "B   BBBB BBBB   B",
            # ~ "B B           B B",
            # ~ "B BBBBBBBBBBBBB B",
            # ~ "B               B",
            # ~ "BBBBBBBB BBBBBBBB",
            # ~ "BBBBBBBBIBBBBBBBB"
        ]

        Level.__init__(self, path)

        wave = Wave(1)

        for x in range(0, 21):
            monster = {
                'monster_type': 'M',
                'name': 'Toto {}'.format(x),
                'hitpoints': 200,
                'speed': 5,
                'entrance': self.get_entrance(),
                'exit': self.get_exit()
            }

            wave.add_monster(monster)

        self.add_wave(wave)


if __name__ == '__main__':
    lvl = DefaultLevel()
    lvl._print_level()